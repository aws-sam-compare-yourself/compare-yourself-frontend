import React, { useState } from 'react'
import { Box, Button, Grid, InputAdornment, TextField, Typography } from '@mui/material'
import { Visibility, VisibilityOff } from '@mui/icons-material'
import { Hoc } from '../../components'
import { login } from '../../services'
import { ACTION, useGlobalState } from '../../core'
import { useNavigate } from 'react-router-dom'


const Login = () => {

    const { dispatch } = useGlobalState()

    const navigate = useNavigate()

    const initialState = {
        username: '',
        password: ''
    }
    const [showPassword, setShowPassword] = useState(false)
    const [input, setInput] = useState(initialState)

    const HandleChange = (event) => {
        const { name, value } = event.target
        setInput(prev => ({
            ...prev,
            [name]: value
        }))
    }

    const HandleSubmit = () => {
        dispatch({ type: ACTION.REQUEST_LOGIN })
        if (input.username && input.password) {
            login(input).then(res => {
                setInput(initialState)
                dispatch({ type: ACTION.LOGIN_SUCCESS, payload: res.idToken })
                navigate('/dashboard')
            }
            ).catch((err) => {
                return err
            })
        }
    }


    return (
        <Grid
            container
            sx={{
                height: '100%',
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center'
            }}>
            <Box
                sx={{
                    width: '600px',
                    height: '500px',
                    padding: '50px',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    gap: '50px',
                    flexDirection: 'column',
                }}
            >
                <Typography variant='h3' sx={{ fontWeight: 500 }}>LOGIN</Typography>

                <Box sx={{
                    width: '400px',
                    fontWeight: 700
                }}>
                    <TextField
                        name='username'
                        size='medium'
                        label='Username'
                        variant='outlined'
                        type={'text'}
                        InputProps={{
                            style: { fontWeight: 550 }
                        }}
                        InputLabelProps={{
                            style: { fontWeight: 600 }
                        }}
                        value={input.username}
                        onChange={(event) => HandleChange(event)}
                        fullWidth />
                </Box>
                <Box sx={{
                    width: '400px'
                }}>
                    <TextField
                        name='password'
                        size='medium'
                        label='Password'
                        id='password'
                        variant='outlined'
                        type={showPassword ? 'text' : 'password'}
                        sx={{
                            width: '100%'
                        }}
                        InputLabelProps={{
                            style: { fontWeight: 600 }
                        }}
                        value={input.password}
                        onChange={(event) => HandleChange(event)}
                        InputProps={{
                            style: { fontWeight: 550 },
                            endAdornment: <InputAdornment sx={{ cursor: 'pointer' }} position='end' onClick={() => setShowPassword(prev => !prev)}>{showPassword ? <Visibility /> : <VisibilityOff />}</InputAdornment>
                        }}
                    />
                </Box>
                <Box sx={{
                    width: '400px',
                    display: 'flex',
                    justifyContent: 'center'
                }}>
                    <Button size='large' variant='contained' sx={{ width: '300px' }} onClick={HandleSubmit}>
                        Submit
                    </Button>
                </Box>
            </Box>
        </Grid>
    )
}

export default Hoc(Login)