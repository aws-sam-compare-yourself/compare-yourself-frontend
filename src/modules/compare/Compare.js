import { Box, Button, Grid, InputAdornment, Stack, TextField, Typography } from '@mui/material'
import React, { useState } from 'react'
import { DataScuccess, Hoc } from '../../components'
import { getData, storeData } from '../../services'

const Compare = () => {

    const initialValue = {
        Age: "",
        Height: "",
        Income: ""
    }

    const [input, setInput] = useState(initialValue)
    const [dataEntered, setDataEntered] = useState(false)
    console.log(input)

    const HandleChange = (e) => {
        e.preventDefault()
        const { name, value } = e.target
        setInput(prev => ({
            ...prev,
            [name]: value
        }))
    }

    const HandleSubmit = () => {
        if (input.Age && input.Height && input.Income) {
            storeData(input).then(res => {
                setDataEntered(prev => !prev)
            }).catch(err => {
                console.log({ err })
            })
        }
    }

    const HandlePrevData = () => {
        getData(false).then(res => {
            console.log({ res })
        }).catch(err => {
            console.log({ err })
        })
    }

    return (
        <>
            {!dataEntered && <Grid
                container
                sx={{
                    height: '100%',
                    padding: '40px',
                    justifyContent: 'center',
                    alignItems: 'center',
                    gap: '250px'
                }}>
                <Typography variant='h3' textAlign={'center'}>Compare YourSelf</Typography>
                <Grid item sx={{
                    flexDirection: 'row',
                    gap: '100px'
                }}>
                    <Box sx={{
                        height: '100px',
                        width: '300px'
                    }}>
                        <TextField label='Age' variant='standard' type={'number'} fullWidth
                            onChange={(e) => HandleChange(e)}
                            name='Age'
                            value={input.Age}
                        />
                    </Box>
                    <Box sx={{
                        height: '100px',
                        width: '300px'
                    }}>
                        <TextField
                            InputProps={{
                                endAdornment: <InputAdornment position='end'>Kg</InputAdornment>
                            }}
                            label='Height'
                            variant='standard'
                            type={'number'}
                            fullWidth
                            onChange={(e) => HandleChange(e)}
                            name='Height'
                            value={input.Height}
                        />
                    </Box>
                    <Box sx={{
                        height: '100px',
                        width: '300px'
                    }}>
                        <TextField
                            InputProps={{
                                endAdornment: <InputAdornment position='end'>Rs</InputAdornment>
                            }}
                            label='Income'
                            variant='standard'
                            type={'number'}
                            fullWidth
                            onChange={(e) => HandleChange(e)}
                            name='Income'
                            value={input.Income}
                        />
                    </Box>
                    <Box sx={{
                        height: '60px',
                        width: '300px',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Button variant='contained' fullWidth onClick={HandleSubmit}>
                            Submit
                        </Button>
                    </Box>
                    <Box sx={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Button onClick={HandlePrevData}>Allready added data</Button>
                    </Box>
                </Grid>
            </Grid>}
            {dataEntered && <DataScuccess data={input} setDataEntered={setDataEntered} setInput={setInput} initialValue={initialValue} />}
        </>
    )
}

export default Hoc(Compare)