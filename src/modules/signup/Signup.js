import React, { useState } from 'react'
import { Box, Button, Grid, IconButton, InputAdornment, InputLabel, OutlinedInput, TextField, Typography } from '@mui/material'
import { InputOutlined, Visibility, VisibilityOff } from '@mui/icons-material'
import { Hoc } from '../../components'
import { confirmUser, signUp } from '../../services'
import { useNavigate } from 'react-router-dom'

const Signup = () => {

    const navigate = useNavigate()

    const initialState = {
        username: '',
        email: '',
        password: '',
        confirmPassword: '',
    }

    const initialValue = {
        username: '',
        otp: ''
    }

    const [showPassword, setShowPassword] = useState(false)
    const [showConfirmPassword, setShowConfirmPassword] = useState(false)
    const [input, setInput] = useState(initialState)
    const [confirmation, setConfirmation] = useState(initialValue)
    const [showConfirm, setShowConfirm] = useState(false)

    const HandleChange = (e) => {
        const { name, value } = e.target
        setInput(prev => ({
            ...prev,
            [name]: value
        }))
    }
    const HandleConfirmChange = (e) => {
        const { name, value } = e.target
        setConfirmation(prev => ({
            ...prev,
            [name]: value
        }))
    }

    const HandleSubmit = (event) => {
        event.preventDefault()
        if (input.username && input.email && input.password && input.confirmPassword) {
            signUp(input).then(() => {
                setInput(initialState)
                setShowConfirm(true)
            }).catch((err) => {
                return err
            })
        } else {
            alert('all fields required')
        }
    }

    const HandleConfirmSubmit = () => {
        if (confirmation.username && confirmation.otp) {
            confirmUser(confirmation).then(() => {
                setShowConfirm(false)
                setConfirmation(initialValue)
                navigate('/login')
            }).catch((err) => {
                alert(err)
            })
        } else {
            alert('all fields required')
        }
    }

    return (
        <Grid
            container
            sx={{
                height: '100%',
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column'
            }}>
            {!showConfirm && <Box
                sx={{
                    width: '600px',
                    height: '500px',
                    padding: '50px',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    gap: '40px',
                    flexDirection: 'column',
                }}
            >
                <Typography variant='h3' sx={{ fontWeight: 500 }}>SIGNUP</Typography>

                <Box sx={{
                    width: '400px',
                    fontWeight: 700
                }}>
                    <TextField
                        name='username'
                        size='small'
                        label='Username'
                        variant='outlined'
                        type={'text'}
                        InputProps={{
                            style: { fontWeight: 550 }
                        }}
                        InputLabelProps={{
                            style: {
                                fontWeight: 600
                            }
                        }}
                        fullWidth
                        value={input.username}
                        onChange={(e) => HandleChange(e)}
                    />
                </Box>
                <Box sx={{
                    width: '400px',
                    fontWeight: 700
                }}>
                    <TextField
                        name='email'
                        size='small'
                        label='Email'
                        variant='outlined'
                        type={'email'}
                        InputProps={{
                            style: { fontWeight: 550 }
                        }}
                        InputLabelProps={{
                            style: { fontWeight: 600 }
                        }}
                        value={input.email}
                        onChange={(e) => HandleChange(e)}
                        fullWidth />
                </Box>
                <Box sx={{
                    width: '400px'
                }}>
                    <TextField
                        name='password'
                        size='small'
                        label='Password'
                        id='password'
                        variant='outlined'
                        type={showPassword ? 'text' : 'password'}
                        sx={{
                            width: '100%'
                        }}
                        InputLabelProps={{
                            style: { fontWeight: 600 }
                        }}
                        value={input.password}
                        onChange={(e) => HandleChange(e)}
                        InputProps={{
                            style: { fontWeight: 550 },
                            endAdornment: <InputAdornment sx={{ cursor: 'pointer' }} position='end' onClick={() => setShowPassword(prev => !prev)}>{showPassword ? <Visibility /> : <VisibilityOff />}</InputAdornment>
                        }}
                    />
                </Box>
                <Box sx={{
                    width: '400px'
                }}>
                    <TextField
                        name='confirmPassword'
                        size='small'
                        label='Confirm Password'
                        id='confirmPassword'
                        variant='outlined'
                        type={showConfirmPassword ? 'text' : 'password'}
                        sx={{
                            width: '100%'
                        }}
                        InputLabelProps={{
                            style: { fontWeight: 600 }
                        }}
                        value={input.confirmPassword}
                        onChange={(e) => HandleChange(e)}
                        InputProps={{
                            style: { fontWeight: 550 },
                            endAdornment: <InputAdornment sx={{ cursor: 'pointer' }} position='end' onClick={() => setShowConfirmPassword(prev => !prev)}>{showPassword ? <Visibility /> : <VisibilityOff />}</InputAdornment>
                        }}
                    />
                </Box>
                <Box sx={{
                    width: '400px',
                    display: 'flex',
                    justifyContent: 'center'
                }}>
                    <Button size='large' variant='contained' sx={{ width: '300px' }} onClick={HandleSubmit}>
                        Submit
                    </Button>
                </Box>
                {!showConfirm && <Box sx={{
                    width: '400px',
                    display: 'flex',
                    justifyContent: 'center',
                    marginTop: '0px'
                }}>
                    <Button size='large' variant='text' sx={{ width: '300px' }} onClick={() => setShowConfirm(true)}>
                        Confirm User
                    </Button>
                </Box>}
            </Box>}
            {showConfirm && <Box
                sx={{
                    width: '600px',
                    height: '300px',
                    padding: '50px',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    gap: '40px',
                    flexDirection: 'column',
                }}
            >
                <Typography variant='h4' sx={{ fontWeight: 500 }}>CONFIRM</Typography>
                <Box sx={{
                    width: '400px',
                    fontWeight: 700
                }}>
                    <TextField
                        name='username'
                        size='small'
                        label='Username'
                        variant='outlined'
                        type={'text'}
                        InputProps={{
                            style: { fontWeight: 550 }
                        }}
                        InputLabelProps={{
                            style: { fontWeight: 600 }
                        }}
                        value={confirmation.username}
                        onChange={(e) => HandleConfirmChange(e)}
                        fullWidth />
                </Box>
                <Box sx={{
                    width: '400px',
                    fontWeight: 700
                }}>
                    <TextField
                        name='otp'
                        size='small'
                        label='OTP'
                        variant='outlined'
                        type={'text'}
                        InputProps={{
                            style: { fontWeight: 550 }
                        }}
                        InputLabelProps={{
                            style: { fontWeight: 600 }
                        }}
                        value={confirmation.otp}
                        onChange={(e) => HandleConfirmChange(e)}
                        fullWidth />
                </Box>
                <Box sx={{
                    width: '400px',
                    display: 'flex',
                    justifyContent: 'center',
                    marginTop: '0px'
                }}>
                    <Button size='large' variant='contained' sx={{ width: '300px' }} onClick={HandleConfirmSubmit}>
                        Confirm User
                    </Button>
                </Box>
            </Box>}
        </Grid>
    )
}

export default Hoc(Signup)