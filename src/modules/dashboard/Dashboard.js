import { Typography } from '@mui/material'
import React from 'react'
import { Hoc } from '../../components'

const Dashboard = () => {
    return (
        <Typography variant='h1' textAlign={'center'}>Successfully Loged into Dashboard...!</Typography>
    )
}

export default Hoc(Dashboard)