import { AppBar, Box, Button, IconButton, Toolbar, Typography } from '@mui/material'
import React from 'react'
import { NavLink } from 'react-router-dom'
import { ACTION, useGlobalState } from '../core'
import { logout } from '../services'

const Navbar = () => {


    const { state, dispatch } = useGlobalState()
    const { user } = state

    const HandleLogout = () => {
        dispatch({ type: ACTION.LOGOUT })
        console.log({ user })
        logout()
    }

    return (
        <AppBar>
            <Toolbar>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    edge="start"
                    sx={{ mr: 2, display: { sm: 'none' } }}
                >
                </IconButton>
                <Typography
                    variant="h6"
                    component="div"
                    sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' }, cursor: 'default', fontWeight: 600 }}
                >
                    Compare Yourself
                </Typography>
                <Box sx={{ display: { xs: 'none', sm: 'flex' }, gap: '20px', marginRight: '40px', }}>
                    {(!user) && <><NavLink to='/login' style={{ textDecoration: 'none' }}>
                        <Button sx={{ color: '#fff' }}>
                            Login
                        </Button>
                    </NavLink>
                        <NavLink to='/signup' style={{
                            textDecoration: 'none'
                        }}>
                            <Button sx={{ color: '#fff' }}>
                                Signup
                            </Button>
                        </NavLink></>}
                    {(user) && <><NavLink to='/compare' style={{
                        textDecoration: 'none'
                    }}>
                        <Button sx={{ color: '#fff' }}>
                            Compare
                        </Button>
                    </NavLink>
                        <NavLink to='/login' style={{
                            textDecoration: 'none'
                        }}>
                            <Button sx={{ color: '#fff' }} onClick={HandleLogout}>
                                Logout
                            </Button>
                        </NavLink>
                    </>}
                </Box>
            </Toolbar>
        </AppBar>
    )
}

export default Navbar