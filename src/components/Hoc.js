import { Grid } from '@mui/material'
import React from 'react'
import Navbar from './Navbar'

const Hoc = (Component) => () => {

    return (
        <Grid container direction={'column'} height={"100%"} width="100%">
            <Grid item height='7%' width='100%'>
                <Navbar />
            </Grid>
            <Grid item height={"93%"} width="100%">
                <Component />
            </Grid>
        </Grid>
    )
}

export default Hoc