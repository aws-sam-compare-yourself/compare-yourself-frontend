
import { createTheme } from "@mui/material";

const customTheme = createTheme({
    typography: {
        fontFamily: 'Ubuntu'
    }
})

export default customTheme