import { Button, Grid, Stack, Typography } from '@mui/material'
import React from 'react'

const DataScuccess = ({ data, initialValue, setDataEntered, setInput }) => {

    const HandleSetData = () => {
        setDataEntered(prev => !prev)
        setInput(initialValue)
    }

    return (
        <Grid container sx={{
            height: '100%',
            padding: '40px',
            justifyContent: 'flex-start',
            alignItems: 'center',
            flexDirection: 'column',
        }}>
            <Typography variant='h4' mb={2}>Your Result</Typography>
            <Stack sx={{
                display: 'flex',
                flexDirection: 'row',
                gap: '10px',
                marginBottom: '100px'
            }}>
                <Button variant='contained' color='success' onClick={HandleSetData}>Set Data</Button>
                <Button variant='contained' color='error'>Clear Data on Server</Button>
                <Button variant='contained' color='primary'>Get Result</Button>
            </Stack>
            <Typography variant='h4'>Select Filter</Typography>
            <Stack sx={{
                width: { xs: '200px', md: '500px' },
                display: 'flex',
                flexDirection: 'column',
                marginTop: '20px'
            }}>
                <Button variant='contained' fullWidth sx={{
                    height: '50px', borderRadius: '0px', backgroundColor: 'transparent', color: 'black', '&:hover': {
                        color: 'white'
                    }
                }}>Your Age : {data.Age}</Button>
                <Button variant='contained' fullWidth sx={{
                    height: '50px', borderRadius: '0px', backgroundColor: 'transparent', color: 'black', '&:hover': {
                        color: 'white'
                    }
                }}>Your Height : {data.Height}</Button>
                <Button variant='contained' fullWidth sx={{
                    height: '50px', borderRadius: '0px', backgroundColor: 'transparent', color: 'black', '&:hover': {
                        color: 'white'
                    }
                }} >Your Income : {data.Income}</Button>
            </Stack>
            <Stack direction={'row'} mt={2} gap={1}>
                <Button variant={'contained'}>Lower is better</Button>
                <Button variant={'contained'}>Higher is better</Button>
            </Stack>
        </Grid>
    )
}

export default DataScuccess