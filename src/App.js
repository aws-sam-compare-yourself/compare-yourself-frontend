import './App.css';
import { CustomTheme } from './components';
import { Compare, Dashboard, Login, Signup } from './modules';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import { ThemeProvider } from '@mui/material';
import { StateProvider } from './core';
import { PrivateRoute, ProtectedRoute } from './route';

function App() {
  return (
    <ThemeProvider theme={CustomTheme}>
      <StateProvider>
        <BrowserRouter>
          <Routes>
            <Route element={<ProtectedRoute />}>
              <Route path='/login' element={<Login />} />
              <Route path='/signup' element={<Signup />} />
              <Route path='/*' element={<Navigate replace to='/compare' />} />
            </Route>
            <Route element={<PrivateRoute />}>
              <Route path='/dashboard' element={<Dashboard />} />
              <Route path='/compare' element={<Compare />} />
              <Route path='/*' element={<Navigate replace to={'/compare'} />} />
            </Route>
          </Routes>
        </BrowserRouter>
      </StateProvider>
    </ThemeProvider>
  );
}

export default App;
