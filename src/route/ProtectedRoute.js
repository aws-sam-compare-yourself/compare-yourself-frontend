import React from 'react'
import { Navigate, Outlet } from 'react-router-dom'

const ProtectedRoute = () => {
    return (
        !JSON.parse(localStorage.getItem('userDetails')) ? <Outlet /> : <Navigate to={'/compare'} />
    )
}

export default ProtectedRoute