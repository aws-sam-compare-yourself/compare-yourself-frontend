const initialStateValue = localStorage.getItem("userDetails") ? JSON.parse(localStorage.getItem("userDetails")) : "";

export const initialState = initialStateValue ? initialStateValue : {
    token: "",
    user: ""
};

export const ACTION = {
    REQUEST_LOGIN: "requestLogin",
    LOGIN_SUCCESS: "loginSuccess",
    LOGOUT: "logout"
}

export const GlobalReducer = (initialState, action) => {
    switch (action.type) {
        case "requestLogin":
            return {
                ...initialState
            }
        case "loginSuccess":
            const stateValue = {
                ...action.payload,
                ...initialState,
                user: action.payload.payload["cognito:username"],
                token: action.payload.jwtToken,
            }
            localStorage.setItem("userDetails", JSON.stringify(stateValue))
            return stateValue;
        case "logout":
            localStorage.removeItem('userDetails')
            return {
                initialState: {},
                user: "",
                token: ""
            }
    }
} 