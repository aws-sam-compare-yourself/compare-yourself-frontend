import React, { createContext, useContext, useReducer } from 'react'
import { GlobalReducer, initialState } from './Reducer';

// creating context
export const ContextData = createContext();

export const useGlobalState = () => {
    const context = useContext(ContextData)
    if (context === undefined) {
        throw new Error("useGlobalState must be within a GlobalProvider")
    }
    return context;
}


// context function
const StateProvider = ({ children }) => {

    // const navigate = useNavigate()

    const [state, dispatch] = useReducer(GlobalReducer, initialState)

    return (
        <>
            <ContextData.Provider value={{ state, dispatch }}>
                {children}
            </ContextData.Provider>
        </>
    )
}

export default StateProvider