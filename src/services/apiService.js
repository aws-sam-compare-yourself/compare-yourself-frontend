import axios from 'axios'
import { getSessionDetails } from './authService'

export const storeData = async (data) => {
    try {

        const session = await getSessionDetails()
        return axios.post('https://bz3y1lpjdh.execute-api.ap-south-1.amazonaws.com/Dev/compare-yourself', data, {
            headers: {
                Authorization: session.getIdToken().getJwtToken(),
                'Content-Type': "application/json",
                'Access-Control-Allow-Origin': "*",
                "Access-Control-Allow-Credentials": true
            }
        })
    } catch (err) {
        return err
    }
}


export const getData = async (all) => {
    try {
        const session = await getSessionDetails()
        const queryParam = `?accessToken=${session.getAccessToken().getJwtToken()}`
        let urlParam = "all";
        if (!all) {
            urlParam = "single"
        }

        return axios.get('https://bz3y1lpjdh.execute-api.ap-south-1.amazonaws.com/Dev/compare-yourself/' + urlParam + queryParam, {
            headers: {
                Authorization: session.getIdToken().getJwtToken(),
                'Content-Type': "application/json",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true
            }
        })

    } catch (err) {
        return err
    }
}