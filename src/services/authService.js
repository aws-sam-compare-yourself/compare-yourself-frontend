import {
    CognitoUserAttribute,
    CognitoUserPool,
    CognitoUser,
    AuthenticationDetails,
} from 'amazon-cognito-identity-js'

const POOL_DATA = {
    UserPoolId: 'ap-south-1_n0O5pNyiy',
    ClientId: '3i3hrljfmhti7s34u92heifh15'
}

const userPool = new CognitoUserPool(POOL_DATA);

export const signUp = (data) => {

    // have to define the attributes in cognito that we given exclude username and password
    const emailAttribute = {
        Name: 'email',
        Value: data.email
    }

    // have to define the attribute array to cognito
    const attrList = []
    attrList.push(new CognitoUserAttribute(emailAttribute))

    return new Promise((resolve, reject) => {
        userPool.signUp(data.username, data.password, attrList, null, (err, result) => {
            if (result) {
                let cognitoUser = result.user;
                console.log({ result })
                console.log('user name is ' + cognitoUser.getUsername());
                resolve(result)
            }
            if (err) {
                reject(err)
            }
        })
    })
}


export const confirmUser = (data) => {
    const userData = {
        Username: data.username,
        Pool: userPool
    }

    const cognitoUser = new CognitoUser(userData);
    return new Promise((resolve, reject) => {
        cognitoUser.confirmRegistration(data.otp, true, (err, result) => {
            if (err) {
                reject(err);
            } resolve(result);
        });
    })
}

export const login = async (data) => {
    const authData = {
        Username: data.username,
        Password: data.password
    }

    const authDetails = new AuthenticationDetails(authData);
    const userData = {
        Username: data.username,
        Pool: userPool
    }

    const cognitoUser = new CognitoUser(userData);
    return new Promise((resolve, reject) => {
        cognitoUser.authenticateUser(authDetails, {
            onSuccess(result) {
                console.log(result)
                resolve(result)
            }, onFailure(err) {
                console.error({ err })
                reject(err)
            }
        })
    })
}

export const getAuthenticatedUser = () => {
    return userPool.getCurrentUser()
}

export const logout = () => {
    getAuthenticatedUser().signOut()
}


export const isAuthenticated = () => {
    const user = getAuthenticatedUser()
    if (!user) {
        return;
    } else {
        user.getSession((err, session) => {
            if (err) {
                alert(err)
            } else {
                if (session.isValid()) {
                    return true
                } else {
                    return false
                }
            }
        })
    }
}


export const getSessionDetails = () => {
    return new Promise((resolve, reject) => {
        getAuthenticatedUser().getSession((err, session) => {
            if (err) {
                reject(err)
            } resolve(session)
        })
    })

}